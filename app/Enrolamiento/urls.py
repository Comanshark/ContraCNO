r"""
RUTAS DE ENROLAMIENTO

Muestra las url's del módulo de enrolamientos del sistema.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.urls import path
from . import views as v


urlpatterns = [
    path('', v.Enrolamientos.as_view(), name='Enrolamientos'),
    path('crear', v.CrearEnrol.as_view(), name='CrearEnrol'),
    path('<int:pk>/editar', v.EditarEnrol.as_view(), name='EditarEnrol'),
    path('<int:pk>/eliminar', v.EliminarEnrol.as_view(), name='EliminarEnrol'),
]
