r""" VISTA DE ENROLAMIENTOS

Se detallan las vistas que controlarán el CRUD de los enrolamientos.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.db.models import Q
from django.utils.timezone import now
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from app.base import views as v
from . import models as m, forms as f


@method_decorator(login_required, 'dispatch')
class Enrolamientos(v.VistaLista):
    """
    Lista los enrolamientos por unidad y equipo.

    Los enroladores podrán ver los enlamientos que hayan registrado en su
    unidad, mientras que los supervisores/encargados podrán ver todos los
    enrolamientos creados en el sistema.
    """

    model = m.Enrolamiento
    template_name = 'enrolamientos.html'
    paginate_by = 10

    def get_queryset(self):
        """ Agrega si lo visualiza un supervisor o un enrolador """
        query = super().get_queryset()

        if not self.request.user.colaborador.es_supervisor:
            query = query.filter(
                unidad=self.request.user.colaborador.enrolador.unidad
            )

        if 'buscar' in self.request.GET and self.request.GET['buscar'] \
                is not None:

            buscar = self.request.GET['buscar']
            query = query.filter(
                Q(primer_recibo__icontains=buscar) |
                Q(ultimo_recibo__icontains=buscar) |
                Q(sede__nombre__icontains=buscar)
            )

        return query


@method_decorator(login_required, 'dispatch')
class CrearEnrol(v.VistaCrear):
    """
    Crea un enrolamiento en el sistema.

    Los enroladores podrán crear un registro de enrolamiento siempre y cuándo
    la sede esté habilitada para ello y la unidad esté asignada.
    """

    model = m.Enrolamiento
    form_class = f.FormularioRecibo
    template_name = 'crear-enrol.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.colaborador.es_supervisor:
            return redirect(reverse_lazy('Equipo:Sedes'))

        self.sede = self.request.user.colaborador.enrolador.unidad. \
            sedes_enrolamiento.filter(fecha=now(), hora_inicio__lte=now(),
                                      hora_final__gt=now()).first()

        if self.sede:
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.warning(
                request, 'No hay sede configurada para el día de '
                'hoy, notifique al supervisor para agregar una.')
            return redirect(reverse_lazy('Equipo:Sedes'))

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form: form_class):
        enrol = form.save()
        enrol.unidad = self.request.user.colaborador.enrolador.unidad
        enrol.sede = self.sede
        enrol.save()

        messages.success(
            self.request,
            'Se ha registrado los enrolamientos satisfactoriamente'
        )

        return redirect(reverse_lazy('Enrolamiento:Enrolamientos'))


@method_decorator(login_required, 'dispatch')
class EditarEnrol(v.VistaEditar):
    """
    Edita un enrolamiento ya existente en el sistema.

    Los enroladores editarán un registro siempre y cuándo estén en el mismo
    día en que lo crearon.
    """

    model = m.Enrolamiento
    form_class = f.FormularioRecibo
    template_name = 'crear-enrol.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.colaborador.es_supervisor:
            return redirect(reverse_lazy('Equipo:Sedes'))

        self.sede = self.request.user.colaborador.enrolador.unidad. \
            sedes_enrolamiento.filter(fecha=now(), hora_inicio__lte=now(),
                                      hora_final__gt=now()).first()

        if self.sede:
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.warning(
                request, 'No hay sede configurada para el día de '
                'hoy, notifique al supervisor para agregar una.')
            return redirect(reverse_lazy('Enrolamiento:Enrolamientos'))

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form: form_class):
        enrol = form.save()
        enrol.sede = self.sede
        enrol.save()

        messages.success(
            self.request,
            'Se ha registrado los enrolamientos satisfactoriamente'
        )

        return redirect(reverse_lazy('Enrolamiento:Enrolamientos'))


@method_decorator(login_required, 'dispatch')
class EliminarEnrol(v.VistaEliminar):
    """
    Elimina un enrolamiento ya existente en el sistema.

    Los enroladores podrán eliminar un registro siempre y cuándo estén en el
    mismo día del enrolamiento.
    """

    model = m.Enrolamiento
    template_name = 'eliminar-enrol.html'
    success_url = reverse_lazy('Enrolamiento:Enrolamientos')

    def dispatch(self, request, *args, **kwargs):
        if request.user.colaborador.es_supervisor:
            return redirect(reverse_lazy('Equipo:Sedes'))

        self.sede = self.request.user.colaborador.enrolador.unidad. \
            sedes_enrolamiento.filter(fecha=now(), hora_inicio__lte=now(),
                                      hora_final__gt=now()).first()

        if self.sede:
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.warning(
                request, 'No hay sede configurada para el día de '
                'hoy, notifique al supervisor para agregar una.')
            return redirect(reverse_lazy('Enrolamiento:Enrolamientos'))
