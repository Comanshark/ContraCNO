r""" FORMULARIO DE ENROLAMIENTO

Detalla los formularios que registrarán los enrolamientos en el sistema.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from datetime import datetime, date
from django import forms as f
from app.base.forms import FormularioBootstrap
from . import models as m

hoy = date.today
strptime = datetime.strptime


class FormularioRecibo(FormularioBootstrap, f.ModelForm):
    """ Formulario para registrar un recibo de enrolamiento """

    # def validar_recibo(self, codigo: str) -> str:
    #   """ Valida el codigo del recibo """

    #   fecha = strptime(codigo[13:21], '%Y%m%d').date()
    #   unidad = m.m.Unidad.objects.filter(
    #       kit=codigo[:4],
    #       sedes_enrolamiento__fecha=fecha
    #   )

    #   if hoy() != fecha:
    #       raise f.ValidationError('El enrolamiento no es del día de hoy')

    #   if unidad.exists():
    #   self.unidad = unidad.first()
    #   else:
    #       raise f.ValidationError(
    #           'La unidad no está habilitada para enrolamiento')

    #   return codigo

    # def clean_primer_recibo(self) -> str:
    #   return self.validar_recibo(self.cleaned_data['primer_recibo'])

    # def clean_ultimo_recibo(self) -> str:
    #   return self.validar_recibo(self.cleaned_data['ultimo_recibo'])

    def save(self) -> m.Enrolamiento:
        """ Añade la información de la estación en la sede """

        # primer_recibo = self.cleaned_data['primer_recibo']
        # ultimo_recibo = self.cleaned_data['ultimo_recibo']
        cantidad_enrol = self.cleaned_data['cantidad_enrol']
        cantidad_actua = self.cleaned_data['cantidad_actua']
        nuevo_enrol = m.Enrolamiento(  # primer_recibo=primer_recibo,
                                       # ultimo_recibo=ultimo_recibo,
                                     cantidad_enrol=cantidad_enrol,
                                     cantidad_actua=cantidad_actua)

        return nuevo_enrol

    class Meta:
        model = m.Enrolamiento
        fields = ('cantidad_enrol', 'cantidad_actua',)
