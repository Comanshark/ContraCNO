""" ADMINISTRACIÓN DE ENROLAMIENTO

Se detallan los modelos de enrolamiento manejados en el sitio administrativo
de Django.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.contrib import admin
from . import models as m


admin.site.register(m.Enrolamiento)
