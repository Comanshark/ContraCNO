""" MODELOS DE ENROLAMIENTO

Se detallan los modelos que registran los enrolamientos.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.db import models
from app.Equipo import models as m


class ManejadorEnrolamiento(models.Manager):
    """ Manejador de centros """

    def por_equipo(self, equipo: m.Equipo):
        return self.filter(sede__equipo=equipo)


class Enrolamiento(models.Model):
    """ Representa los enrolamientos por sede """

    id = models.BigAutoField(primary_key=True)
    sede = models.ForeignKey(m.Sede, on_delete=models.CASCADE,
                             related_name='enrolamientos')
    # primer_recibo = models.CharField(max_length=30, unique=True)
    # ultimo_recibo = models.CharField(max_length=30, unique=True)
    cantidad_enrol = models.PositiveIntegerField('Cantidad de enrolados')
    cantidad_actua = models.PositiveIntegerField('Cantidad de actualizaciones')
    unidad = models.ForeignKey(m.Unidad, on_delete=models.CASCADE,
                               related_name='enrolamientos')

    objects = ManejadorEnrolamiento()

    @property
    def equipo(self):
        return self.sede.equipo

    def __str__(self):
        return f'[Equipo: {self.unidad.equipo.nombre}] ' \
               f'Unidad {self.unidad.nombre} {self.sede.fecha}'

    class Meta:
        ordering = ('-sede__fecha',)
