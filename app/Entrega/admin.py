""" ADMINISTRACIÓN DE ENTREGA

Se detallan todos los modelos de entrega para el sitio administrativo de
Django.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.contrib import admin
from . import models as m


admin.site.register(m.Centro)
admin.site.register(m.Caja)
admin.site.register(m.Sobre)
