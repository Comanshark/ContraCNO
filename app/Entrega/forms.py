""" FORMULARIOS DE ENTREGA DE DNI
Se muestran los formularios para la edición del módulo de entregas.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

import re
from app.base.forms import FormularioBootstrap, f
from . import models as m


SOBRE_REGEX = r'^[0-9]{9}$'
CAJA_REGEX = r'^P[0-9]{3}B[0-9]{2}$'


class FormCentro(FormularioBootstrap, f.ModelForm):
    """ Formulario para la edición de centros """

    def save(self, commit: bool = True):
        """ Agrega el equipo al formulario """

        centro = super().save(commit=False)
        centro.equipo = self.equipo

        if commit:
            centro.save()

        return centro

    class Meta:
        model = m.Centro
        fields = ('nombre',)


class FormCajaBase(FormularioBootstrap, f.ModelForm):
    """ Formulario para la edición de cajas """

    def __init__(self, equipo: m.m.Equipo, editable: bool = False,
                 *args, **kwargs):
        """ Añade un filtro al queryset y texto de edición de sobres"""

        super().__init__(equipo, editable, *args, **kwargs)

        if not editable:
            self.fields['sobres'] = f.CharField(
                label='Sobres', widget=f.Textarea(
                    attrs={'cols': 1, 'rows': 10, 'class': 'form-control'}))

    def clean_sobres(self):
        """ Valida si los sobres tienen el formato correcto """

        sobres = list
        sobres = self.cleaned_data['sobres'].split('\r\n')

        for sobre in sobres:
            if re.search(SOBRE_REGEX, sobre) is None:
                self.add_error('sobres', f.ValidationError(
                    f'El código «{sobre}» no es válido'))
        return sobres

    def clean_codigo(self):
        """ Validación de código de caja """

        codigo = self.cleaned_data['codigo']

        # Verifica que el código cumpla con los parámetros.
        if re.search(CAJA_REGEX, codigo) is None:
            raise f.ValidationError('El código de la caja no cumple '
                                    'los parámetros')

        return codigo

    class Meta:
        model = m.Caja
        exclude = ('cantidad_listado',)


class FormCajaInventario(FormCajaBase):

    def save(self):
        """
        Elimina los sobres que no existen en físico y no están entregados.
        """

        caja = str
        codigo_sobres = list
        sobres_eliminar = list

        caja = self.cleaned_data['codigo']
        codigo_sobres = list(set(self.cleaned_data['sobres']))

        sobres_eliminar = m.Sobre.objects.filter(caja__codigo=caja) \
            .exclude(codigo__in=codigo_sobres) \
            .exclude(dato_entrega__isnull=False)

        return sobres_eliminar

    class Meta(FormCajaBase.Meta):
        exclude = ('cantidad_listado', 'centro',)


class FormCajaCrear(FormCajaBase):

    def __init__(self, equipo: m.m.Equipo, editable: bool = False,
                 *args, **kwargs):
        """ Añade un filtro al queryset y texto de edición de sobres"""

        super().__init__(equipo, editable, *args, **kwargs)
        self.fields['centro'].queryset = m.Centro.objects.por_equipo(equipo)

    def save(self, commit=True):
        """ Crea los sobres de la caja """

        caja = m.Caja
        codigos_sobres = list
        sobres = list()

        caja = super().save(commit=False)
        caja.save()

        if not self.editable:
            codigos_sobres = list(set(self.cleaned_data['sobres']))

            for codigo in codigos_sobres:
                sobre = m.Sobre(codigo=codigo, caja=caja)
                sobre.save()
                sobres.append(sobre)

        return caja


class FormSobreBase(FormularioBootstrap):
    """ Formulario base para creación/edición de sobres y entregas """

    sobre = f.CharField(max_length=9, min_length=9, required=True,
                        label='Código de caja')
    caja = f.CharField(max_length=7, min_length=7, required=True,
                       label='Código de sobre')

    def clean_caja(self):
        """ Verifica que la caja cumple con los parámetros"""

        codigo = self.cleaned_data['caja']

        if re.search(CAJA_REGEX, codigo) is None:
            raise f.ValidationError('El código de la caja no cumple '
                                    'los parámetros')

        return codigo

    def clean_sobre(self):
        """ Verifica si el código cumple los parámetros"""

        codigo = self.cleaned_data['sobre']

        if re.search(SOBRE_REGEX, codigo) is None:
            raise f.ValidationError('El código del sobre no cumple los '
                                    'parámetros')

        return codigo

    def clean(self):
        """ Verifica que la caja ya existe en el sistema """

        datos = super().clean()

        if 'caja' in datos and datos['caja'] is not None:
            # Verifica si la caja existe y es parte del equipo
            codigo_caja = datos['caja']
            caja = m.Caja.objects.por_equipo(self.equipo).filter(
                codigo=codigo_caja)

            if caja.exists():
                self.caja = list(caja)
            else:
                self.add_error('caja', f.ValidationError(
                    'La caja no existe en el sistema'))
                self.caja = None

            self._validar_campos()

            return datos


class FormSobre(FormSobreBase, f.ModelForm):
    """ Formulario para la creación de sobres """

    def clean(self):
        datos = super().clean()

        if 'sobre' in datos and datos['sobre'] is not None:
            sobre = m.Sobre.objects.filter(codigo=datos['sobre'],
                                           caja__in=self.caja)

            if sobre.exists():
                self.add_error('sobre', f.ValidationError(
                    'El sobre ya existe en el sistema'))

        self._validar_campos()

    def save(self, commit=True):
        """ Crea el sobre """

        codigo_sobre = self.cleaned_data['sobre']
        sobre = m.Sobre(codigo=codigo_sobre, caja=self.caja[0])

        if commit:
            sobre.save()

        return sobre

    class Meta:
        model = m.Sobre
        exclude = ('codigo', 'caja',)


class FormEntrega(FormSobreBase, f.ModelForm):
    """ Formulario para la creación de entrega de sobres """

    def clean(self):
        """ Verifica si el sobre existe en el sistema """

        datos = super().clean()

        if 'sobre' in datos and datos['sobre'] is not None:
            codigo_sobre = datos['sobre']
            sobre = m.Sobre.objects.filter(codigo=codigo_sobre,
                                           caja__in=self.caja)

            if sobre.exists():
                self.sobre = sobre.first()
            else:
                self.add_error('sobre', f.ValidationError(
                    'El sobre no existe en el sistema'))
                self.sobre = None

            self._validar_campos()

            return datos

    def save(self, commit=True):
        """ Crea la entrega del sobre """

        entrega = m.EntregaSobre(sobre=self.sobre)

        if commit:
            entrega.save()

        return entrega

    class Meta:
        model = m.EntregaSobre
        exclude = ('sobre', 'sede_entrega', 'enrolador_entrega',
                   'enrolador_escaner',)


class FormEntregaEscaner(FormEntrega):
    """ Formulario para la creacion de sobres por un escaneador """

    def __init__(self, equipo: m.m.Equipo, editable=False, *args, **kwargs):
        """ Añade la lista de enroladores """

        super().__init__(equipo, editable, *args, **kwargs)

        self.fields['enrolador_entrega'].queryset = m.m.Enrolador.objects \
            .por_equipo(equipo)

    def save(self, commit=True):
        """ Añade el enrolador seleccionado """

        entrega = super().save(commit=False)
        entrega.enrolador_entrega = self.cleaned_data['enrolador_entrega']

        if commit:
            entrega.save()

        return entrega

    class Meta:
        model = m.EntregaSobre
        fields = ('enrolador_entrega',)
