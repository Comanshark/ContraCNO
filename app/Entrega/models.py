""" MODELOS DE ENTREGA

Se detallan los modelos para el inventario de entrega de DNIs.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.db import models
from app.Equipo import models as m


class ManejadorCentro(models.Manager):
    """ Manejador de centros """

    def por_equipo(self, equipo: m.Equipo):
        return self.filter(equipo=equipo)


class Centro(models.Model):
    """ Representa un centro de entrega """

    id = models.BigAutoField(primary_key=True)
    equipo = models.ForeignKey(m.Equipo, on_delete=models.CASCADE,
                               related_name='centros')
    nombre = models.CharField(max_length=100, unique=False)

    objects = ManejadorCentro()
    _total_sobres = 0

    def __str__(self):
        return f'(Equipo: {self.equipo.nombre}) {self.nombre}'

    @property
    def total_sobres(self):
        """ Devuelve el número de sobres entregados """

        if not self._total_sobres:
            for cajas in self.cajas.all():
                self._total_sobres += cajas.sobres \
                    .filter(dato_entrega__isnull=False).count()

        return self._total_sobres

    class Meta:
        ordering = ('nombre',)


class ManejadorCaja(models.Manager):
    """ Manejador de cajas """

    def por_equipo(self, equipo: m.Equipo):
        return self.filter(centro__equipo=equipo)


class Caja(models.Model):
    """ Representa una caja de sobres """

    id = models.BigAutoField(primary_key=True)
    codigo = models.CharField(max_length=7, unique=False)
    centro = models.ForeignKey(Centro, on_delete=models.CASCADE,
                               related_name='cajas')
    cantidad_listado = models.PositiveIntegerField(default=0)

    objects = ManejadorCaja()

    @property
    def cantidad(self):
        """ Retorna la cantidad de sobres en la caja """
        return self.cantidad_listado if self.cantidad_listado != 0 \
            else self.sobres.count()

    @property
    def entregados(self):
        """ Retorna la cantidad de sobres entregadas """
        return self.sobres.filter(dato_entrega__isnull=False).count()

    @property
    def equipo(self) -> m.Equipo:
        return self.centro.equipo

    def __str__(self):
        return f'(Equipo: {self.equipo.nombre}) {self.codigo}'

    class Meta:
        ordering = ('codigo',)


class ManejadorSobre(models.Manager):
    """ Manejador de sobres """

    def por_equipo(self, equipo: m.Equipo):
        return self.filter(caja__centro__equipo=equipo)


class Sobre(models.Model):
    """ Representa un sobre de DNI """

    id = models.BigAutoField(primary_key=True)
    codigo = models.CharField(max_length=9, unique=False)
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE,
                             related_name='sobres')

    objects = ManejadorSobre()

    @property
    def entregado(self):
        entregado = bool

        try:
            entregado = self.dato_entrega is not None
        except models.ObjectDoesNotExist:
            entregado = False

        return entregado

    @property
    def equipo(self) -> m.Equipo:
        return self.caja.centro.equipo

    def __str__(self):
        return f'(Equipo: {self.equipo.nombre}) ' \
               f'{self.codigo}-{self.caja.codigo}'

    class Meta:
        ordering = ('codigo',)


class ManejadorEntrega(models.Manager):
    """ Manejador de entregas """

    def por_equipo(self, equipo: m.Equipo):
        return self.filter(sobre__caja__centro__equipo=equipo)


class EntregaSobre(models.Model):
    """ Representa la entrega de un sobre en el sistema """

    id = models.BigAutoField(primary_key=True)
    sobre = models.OneToOneField(Sobre, on_delete=models.CASCADE,
                                 related_name='dato_entrega')
    hora_entrega = models.TimeField(auto_now_add=True)
    sede_entrega = models.ForeignKey(m.Sede, on_delete=models.CASCADE,
                                     related_name='sobres_entregados')
    enrolador_entrega = models.ForeignKey(m.Enrolador,
                                          on_delete=models.CASCADE,
                                          related_name='sobres_entregados')
    enrolador_escaner = models.ManyToManyField(
        m.Enrolador, blank=True,
        editable=False,
        related_name='sobres_escaneados'
    )

    objects = ManejadorEntrega()

    @property
    def fecha_entrega(self):
        return self.sede_entrega.fecha

    @property
    def equipo(self) -> m.Equipo:
        return self.sobre.caja.centro.equipo

    class Meta:
        ordering = ('-sede_entrega__fecha', '-hora_entrega',)
