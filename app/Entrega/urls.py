""" RUTAS DE ENTREGA DE DNI

Se detallan todas las rutas del módulo de entregas.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.urls import path
from . import views as v


urlpatterns = [
    path('actual', v.datos_actualizados, name='Actualizados'),
    path('centros', v.Centros.as_view(), name='Centros'),
    path('centros/crear', v.CrearCentro.as_view(), name='CrearCentro'),
    path('centros/<int:pk>/editar', v.EditarCentro.as_view(),
         name='EditarCentro'),
    path('centros/<int:pk>/eliminar', v.EliminarCentro.as_view(),
         name='EliminarCentro'),
    path('cajas', v.Cajas.as_view(), name='Cajas'),
    path('cajas/crear', v.CrearCaja.as_view(), name='CrearCaja'),
    path('cajas/<int:pk>', v.VerCaja.as_view(), name='VerCaja'),
    path('cajas/<int:pk>/editar', v.EditarCaja.as_view(), name='EditarCaja'),
    path('cajas/<int:pk>/eliminar', v.EliminarCaja.as_view(),
         name='EliminarCaja'),
    path('cajas/inventario', v.InventarioCaja.as_view(),
         name='InventarioCaja'),
    path('entregas', v.Entregas.as_view(), name='Entregas'),
    path('entregas/crear', v.CrearEntrega.as_view(), name='CrearEntrega'),
    path('entregas/crear-escaner', v.CrearEntregaEscaner.as_view(),
         name='CrearEntregaEscaner'),
    path('entregas/<int:pk>/eliminar', v.EliminarEntrega.as_view(),
         name='EliminarEntrega'),
    path('sobres/crear', v.CrearSobre.as_view(), name='CrearSobre'),
    path('sobres/<int:pk>/eliminar', v.EliminarSobre.as_view(),
         name='EliminarSobre'),
]
