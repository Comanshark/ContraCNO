""" VISTAS DE ENTREGA DNI

Se detallan todas las vistas para entregas de DNI.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.http import HttpResponse
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.utils.timezone import now
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from app.base import views as v
from . import models as m, forms as f


@login_required
def datos_actualizados(request, *args, **kwargs):
    """ Retorna datos actualizados del enrolador """

    if not request.user.colaborador.es_supervisor:
        enrolador = request.user.colaborador.enrolador

        entregados = enrolador.sobres_entregados.filter(
            sede_entrega__fecha=now().date()).count()
        enrolados = enrolador.unidad.enrolamientos.filter(
            sede__fecha=now().date())
        enrolados = enrolados.first().cantidad_enrol + \
            enrolados.first().cantidad_actua if enrolados else 0
        escaneados = enrolador.sobres_escaneados.filter(
            sede_entrega__fecha=now().date()).count()

        return HttpResponse(str([entregados, enrolados, escaneados]))
    else:
        return HttpResponse(str([0, 0, 0]))


@method_decorator(login_required, 'dispatch')
class Centros(v.VistaLista):
    """ Muestra una lista de todos los centros """

    model = m.Centro
    template_name = 'centros.html'
    paginate_by = 10

    def get_queryset(self):
        """ Añade términos de búsqueda """

        query = super().get_queryset()

        if 'buscar' in self.request.GET and self.request.GET['buscar'] \
                is not None:

            buscar = self.request.GET['buscar']
            query = query.filter(nombre__icontains=buscar)

        return query


@method_decorator(login_required, 'dispatch')
class CrearCentro(v.VistaCrear):
    """ Crea un nuevo centro en el sistema """

    model = m.Centro
    template_name = 'crear-centro.html'
    form_class = f.FormCentro
    success_url = reverse_lazy('Entrega:Centros')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se registró el centro correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EditarCentro(v.VistaEditar):
    """ Edita un centro en el sistema """

    model = m.Centro
    template_name = 'crear-centro.html'
    form_class = f.FormCentro
    success_url = reverse_lazy('Entrega:Centros')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se editó el centro correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EliminarCentro(v.VistaEliminar):
    """ Elimina un centro del sistema """

    model = m.Centro
    template_name = 'eliminar-centro.html'
    success_url = reverse_lazy('Entrega:Centros')


@method_decorator(login_required, 'dispatch')
class VerCaja(v.VistaVer):
    """ Visualiza los detalles de una caja en el sistema """

    model = m.Caja
    template_name = 'ver-caja.html'

    def obtener_sobres(self):
        """ Obtiene la lista de sobres de la caja """

        sobres = m.Sobre.objects.filter(caja=self.get_object())

        if 'buscar' in self.request.GET \
                and self.request.GET['buscar'] is not None:
            sobres = sobres.filter(
                codigo__icontains=self.request.GET['buscar'])

        return sobres

    def get_context_data(self, **kwargs):
        """ Añade la lista de sobres de la caja """

        ctx = super().get_context_data(**kwargs)
        sobres = self.obtener_sobres()
        pagina = Paginator(sobres, 10)

        if 'page' in self.request.GET and self.request.GET['page'] is not None:
            ctx['page_obj'] = pagina.get_page(self.request.GET['page'])
        else:
            ctx['page_obj'] = pagina.get_page(1)

        return ctx


@method_decorator(login_required, 'dispatch')
class Cajas(v.VistaLista):
    """ Muestra una lista de cajas """

    model = m.Caja
    template_name = 'cajas.html'
    paginate_by = 10

    def get_queryset(self):
        """ Agrega los términos de búsqueda del formulario """

        query = super().get_queryset()

        if 'buscar' in self.request.GET and self.request.GET['buscar'] \
                is not None:

            buscar = self.request.GET['buscar']
            query = query.filter(
                Q(codigo__icontains=buscar)
                | Q(centro__nombre__icontains=buscar))

        return query


@method_decorator(login_required, 'dispatch')
class CrearCaja(v.VistaCrear):
    """ Crea una caja en el sistema """

    model = m.Caja
    template_name = 'crear-caja.html'
    form_class = f.FormCajaCrear
    success_url = reverse_lazy('Entrega:Cajas')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """
        messages.success(self.request, 'Se creó la caja correctamente')

        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EditarCaja(v.VistaEditar):
    """ Edita una caja en el sistema """

    model = m.Caja
    template_name = 'crear-caja.html'
    form_class = f.FormCajaCrear
    success_url = reverse_lazy('Entrega:Cajas')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          editable=True,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se editó la caja correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EliminarCaja(v.VistaEliminar):
    """ Elimina una caja en el sistema """

    model = m.Caja
    template_name = 'eliminar-caja.html'
    success_url = reverse_lazy('Entrega:Cajas')


@method_decorator(login_required, 'dispatch')
class InventarioCaja(FormView):
    """ Hace un inventario final de la caja """

    form_class = f.FormCajaInventario
    template_name = 'crear-caja.html'

    def form_valid(self, form):
        sobres = form.save()
        sobres.delete()

        messages.success(self.request, 'Inventario de caja exitoso')

        return redirect(reverse_lazy('Entrega:InventarioCaja'))

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          editable=False,
                          **self.get_form_kwargs())


@method_decorator(login_required, 'dispatch')
class Entregas(v.VistaLista):
    """ Muestra una lista de sobres """

    model = m.EntregaSobre
    template_name = 'entregas.html'
    paginate_by = 10

    def get_queryset(self):
        """ Agrega los términos de búsqueda del formulario """

        query = super().get_queryset()

        if not self.request.user.colaborador.es_supervisor:
            query = query.filter(
                enrolador_entrega=self.request.user.colaborador.enrolador)

        if 'buscar' in self.request.GET and self.request.GET['buscar'] \
                is not None:

            buscar = self.request.GET['buscar']
            query = query.filter(
                Q(sobre__codigo__icontains=buscar) |
                Q(sobre__caja__codigo__icontains=buscar) |
                Q(sobre__caja__centro__nombre__icontains=buscar))

        return query


@method_decorator(login_required, 'dispatch')
class CrearEntrega(v.VistaCrear):
    """ Crea un sobre en el sistema """

    model = m.EntregaSobre
    template_name = 'crear-sobre.html'
    form_class = f.FormEntrega
    sede = m.m.Sede
    success_url = reverse_lazy('Entrega:CrearEntrega')

    def dispatch(self, request, *args, **kwargs):
        """ Alerta si hay sede para trabajar en entregas """

        if request.user.colaborador.es_supervisor:
            return redirect(reverse_lazy('Equipo:Sedes'))

        self.sede = self.request.user.colaborador.enrolador.sedes_entrega \
            .filter(fecha=now(), hora_inicio__lte=now(),
                    hora_final__gt=now()).first()

        if self.sede:
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.warning(
                request, 'No hay sede configurada para el día de '
                'hoy, notifique al supervisor para agregar una.')
            return redirect(reverse_lazy('Equipo:Sedes'))

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form: form_class):
        entrega = form.save(commit=False)
        entrega.sede_entrega = self.sede
        sobre = m.Sobre.objects.get(codigo=entrega.sobre.codigo,
                                    caja__codigo=entrega.sobre.caja.codigo)

        if sobre.entregado:
            messages.warning(
                self.request, 'El sobre ya ha sido entregado, '
                'notifique a su supervisor para solventarlo.')
        else:
            self.entregar_sobre(entrega)

        return redirect(self.success_url)

    def entregar_sobre(self, entrega: m.EntregaSobre):
        """ valida la entrega de un sobre """

        enrolador = self.request.user.colaborador.enrolador
        entrega.enrolador_entrega = enrolador
        entrega.save()
        messages.success(self.request, 'El sobre ha sido entregado '
                         'correctamente')


@method_decorator(login_required, 'dispatch')
class CrearEntregaEscaner(CrearEntrega):
    """ Crea una entrega cuando un enrolador escanea a otro """

    form_class = f.FormEntregaEscaner
    template_name = 'crear-sobre-escaner.html'
    success_url = reverse_lazy('Entrega:CrearEntregaEscaner')

    def entregar_sobre(self, entrega: m.EntregaSobre):
        """ Valida la entrega de un sobre cuando es escaneado por otro """

        enrolador = self.request.user.colaborador.enrolador
        entrega.save()
        if not enrolador == entrega.enrolador_entrega:
            entrega.enrolador_escaner.set((enrolador, ))
        messages.success(
            self.request, f'El sobre {entrega.sobre.codigo} ha '
            'sido entregado correctamente por '
            f'{entrega.enrolador_entrega}.')


@method_decorator(login_required, 'dispatch')
class EliminarEntrega(v.VistaEliminar):
    """ Elimina una entrega del sistema """

    model = m.EntregaSobre
    template_name = 'eliminar-entrega.html'
    success_url = reverse_lazy('Entrega:Entregas')

    def puede_eliminarlo(self) -> bool:
        colaborador = self.request.user.colaborador
        entrega = self.get_object()

        return colaborador.es_supervisor or \
            colaborador.enrolador == entrega.enrolador_entrega


@method_decorator(login_required, 'dispatch')
class CrearSobre(v.VistaCrear):
    """ Crea un sobre de una caja """

    model = m.Sobre
    form_class = f.FormSobre
    template_name = 'crear-sobre.html'

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        sobre = form.save(commit=False)
        sobre.save()
        messages.success(self.request,
                         'El sobre ha sido guardado exitosamente')

        return redirect(
            reverse_lazy('Entrega:VerCaja', kwargs={'pk': sobre.caja.pk}))


@method_decorator(login_required, 'dispatch')
class EliminarSobre(v.VistaEliminar):
    """ Elimina un sobre de una caja """

    model = m.Sobre
    template_name = 'eliminar-sobre.html'

    def dispatch(self, request, *args, **kwargs):
        self.caja = self.get_object().caja
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('Entrega:VerCaja', kwargs={'pk': self.caja.pk})
