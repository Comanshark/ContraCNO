# Generated by Django 2.2 on 2021-06-24 00:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Equipo', '0004_auto_20210615_1304'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sede',
            options={'ordering': ('-fecha', '-es_domiciliaria', '-hora_inicio'), 'verbose_name': 'sede', 'verbose_name_plural': 'sedes'},
        ),
    ]
