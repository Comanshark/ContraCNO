""" RUTAS DE EQUIPO

Se detallan las rutas de acceso de los equipos.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.urls import path
from . import views as v

urlpatterns = [
        path('sedes', v.Sedes.as_view(), name='Sedes'),
        path('sedes/crear', v.CrearSede.as_view(), name='CrearSede'),
        path('sedes/<int:pk>', v.VerSede.as_view(), name='VerSede'),
        path('sedes/<int:pk>/editar', v.EditarSede.as_view(),
             name='EditarSede'),
        path('sedes/<int:pk>/eliminar', v.EliminarSede.as_view(),
             name='EliminarSede'),
        path('domiciliaria/crear', v.CrearDomiciliaria.as_view(),
             name='CrearDomiciliaria'),
        path('domiciliaria/<int:pk>/editar', v.EditarDomiciliaria.as_view(),
             name='EditarDomiciliaria'),
]
