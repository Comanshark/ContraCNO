""" ADMINISTRACIÓN DE EQUIPO

Muestra la administración del equipo en Django.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.contrib import admin
from . import models as m


admin.site.register(m.Supervisor)
admin.site.register(m.Enrolador)
admin.site.register(m.Equipo)
admin.site.register(m.Unidad)
admin.site.register(m.Sede)
