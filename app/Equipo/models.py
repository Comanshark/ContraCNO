""" MODOELOS DE EQUIPO

Definen los elementos básicos de un equipo en el proyecto Identifícate.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Colaborador(models.Model):
    """ Representa un supervisor en el sistema """

    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    usuario = models.OneToOneField(User, on_delete=models.CASCADE,
                                   related_name='colaborador')
    es_supervisor = models.BooleanField(editable=False, default=False)

    @property
    def nbreve(self):
        nombre = self.nombre
        return nombre.split(' ')[0]

    @property
    def abreve(self):
        return self.apellido.split(' ')[0]

    @property
    def equipo(self):
        equipo = None

        if self.es_supervisor:
            try:
                equipo = self.supervisor.equipo
            except models.ObjectDoesNotExist:
                pass
        else:
            equipo = self.enrolador.equipo

        return equipo

    def __str__(self):
        return f'{self.nombre} {self.apellido}'


class Supervisor(Colaborador):
    """ Representa a un supervisor del proyecto """

    def save(self):
        """ Marca como supervisor """

        self.es_supervisor = True
        super().save()

    def __str__(self):
        return f'{self.nombre} {self.apellido}'

    class Meta:
        verbose_name = 'supervisor'
        verbose_name_plural = 'supervisores'


class Equipo(models.Model):
    """ Representa un equipo del proyecto """

    id = models.BigAutoField(primary_key=True)
    supervisor = models.OneToOneField(Supervisor, on_delete=models.CASCADE,
                                      related_name='equipo')
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.nombre} (Supervisor: {self.supervisor.nombre})'

    class Meta:
        verbose_name = 'equipo'
        verbose_name_plural = 'equipos'


class ManejadorUnidad(models.Manager):
    """ Manejador de unidad """

    def por_equipo(self, equipo: Equipo):
        return self.filter(equipo=equipo)


class Unidad(models.Model):
    """ Representa una UME en el sistema """

    id = models.BigAutoField(primary_key=True)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE,
                               related_name='unidades')
    nombre = models.CharField(max_length=50, blank=True)
    kit = models.CharField(max_length=4, blank=True)

    objects = ManejadorUnidad()

    def __str__(self):
        return f'Unidad {self.nombre}'

    class Meta:
        verbose_name = 'UME'
        verbose_name_plural = 'UMEs'


class ManejadorEnrolador(models.Manager):
    """ Manejador de enroladores """

    def por_equipo(self, equipo: Equipo):
        return self.filter(unidad__equipo=equipo)


class Enrolador(Colaborador):
    """ Representa un enrolador en el sistema """

    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE,
                               related_name='enroladores')
    usuario_dominio = models.CharField(max_length=50, blank=True)

    objects = ManejadorEnrolador()

    @property
    def equipo(self):
        return self.unidad.equipo

    def save(self):
        """ Marca como no supervisor """

        self.es_supervisor = False
        super().save()

    class Meta:
        verbose_name = 'enrolador'
        verbose_name_plural = 'enroladores'


class ManejadorSede(models.Manager):
    """ Manejador de sedes """

    def por_equipo(self, equipo: Equipo):
        return self.filter(equipo=equipo)


class Sede(models.Model):
    """ Representa una sede de entrega/enrolamiento """

    id = models.BigAutoField(primary_key=True)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE,
                               related_name='sedes')
    fecha = models.DateField(default=timezone.now)
    hora_inicio = models.TimeField()
    hora_final = models.TimeField()
    nombre = models.CharField(max_length=100, unique=False, blank=True)
    lugar = models.CharField(max_length=100, unique=False, blank=True)
    es_domiciliaria = models.BooleanField(default=False)

    enroladores = models.ManyToManyField(Unidad, blank=True,
                                         related_name='sedes_enrolamiento')
    entregadores = models.ManyToManyField(Enrolador, blank=True,
                                          related_name='sedes_entrega')

    objects = ManejadorSede()

    @property
    def total_entregadas(self):
        """ Retorna el total de DNIs entregadas """

        return self.sobres_entregados.count()

    @property
    def total_enrolados(self):
        """ Retorna el total de enrolamientos """
        from functools import reduce

        enrolamientos = self.enrolamientos.all().values('cantidad_enrol')

        return reduce(lambda acum, val: acum + val['cantidad_enrol'],
                      enrolamientos, 0)

    @property
    def total_actualizados(self):
        """ Retorna el total de enrolamientos """
        from functools import reduce

        enrolamientos = self.enrolamientos.all().values('cantidad_actua')

        return reduce(lambda acum, val: acum + val['cantidad_actua'],
                      enrolamientos, 0)

    def __str__(self):
        return f'[Equipo: {self.equipo.nombre}] {self.nombre} de {self.lugar}'

    class Meta:
        verbose_name = 'sede'
        verbose_name_plural = 'sedes'
        ordering = ('-fecha', '-es_domiciliaria', '-hora_inicio',)
