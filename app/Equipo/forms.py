""" FORMULARIO DE EQUIPO

Se detallan todos los formularios de los equipos del proyecto.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from app.base.forms import FormularioBootstrap, f
from . import models as m


class FormDomiciliaria(FormularioBootstrap, f.ModelForm):
    """ Formulario de edición de domiciliarias """

    def __init__(self, equipo: m.Equipo, editable: bool = False,
                 *args, **kwargs):
        """ Añadiendo soporte de horas en TimeFields """

        super().__init__(equipo, editable, *args, **kwargs)

        self.fields['hora_inicio'].input_formats = self.fields['hora_final'] \
            .input_formats = ('%I:%M %p',)
        self.fields['enroladores'].queryset = m.Unidad.objects.por_equipo(
            self.equipo)
        self.fields['entregadores'].queryset = m.Enrolador.objects.por_equipo(
            self.equipo)

    class Meta:
        model = m.Sede
        fields = ('nombre', 'lugar', 'fecha', 'hora_inicio', 'hora_final',
                  'enroladores', 'entregadores',)
        widgets = {
                'hora_inicio': f.TimeInput(
                    attrs={'class': 'datetimepicker-input',
                           'data-target': '#hora_inicio'}),

                'hora_final': f.TimeInput(
                    attrs={'class': 'datetimepicker-input',
                                    'data-target': '#hora_final'}),

                'fecha': f.DateInput(
                    attrs={'class': 'datetimepicker-input',
                           'data-target': '#fecha'})
        }

    def save(self, commit: bool = True) -> m.Sede:
        """ Asigna el equipo del usuario """

        sede = super().save(commit=False)
        sede.equipo = self.equipo
        sede.es_domiciliaria = True
        sede.save()

        self.save_m2m()

        if commit:
            sede.save()

        return sede


class FormSede(FormDomiciliaria):
    """ Formulario de edición de sedes """

    def __init__(self, equipo: m.Equipo, editable: bool = False,
                 *args, **kwargs):
        """ Añade un filtro de gente que está en otra zona """

        super().__init__(equipo, editable, *args, **kwargs)

    def save(self, commit: bool = True) -> m.Sede:
        """ Asigna la sede como no domiciliaria """

        sede = super().save(commit=False)
        sede.es_domiciliaria = False
        sede.save()
        print(sede.es_domiciliaria)

        if commit:
            sede.save()

        return sede
