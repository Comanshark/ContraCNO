""" VISTAS DE EQUIPO

Se detallan las vistas que están relacionadas con los equipos del proyecto.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from functools import reduce
from django.urls import reverse_lazy
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from app.base import views as v
from . import models as m, forms as f
from app.Entrega.models import EntregaSobre
from app.Enrolamiento.models import Enrolamiento


@method_decorator(login_required, 'dispatch')
class Inicio(v.VistaPlantilla):
    template_name = 'base.html'


@method_decorator(login_required, 'dispatch')
class Sedes(v.VistaLista):
    """ Obtiene el listado de sedes de un equipo """

    model = m.Sede
    template_name = 'sedes.html'
    paginate_by = 10


@method_decorator(login_required, 'dispatch')
class VerSede(v.VistaVer):
    """ Visualiza los datos de una sede """

    model = m.Sede
    template_name = 'ver-sede.html'

    def get_context_data(self, **kwargs):
        """ Añade los datos de entregas por enrolador """

        ctx = super().get_context_data(**kwargs)
        equipo = self.request.user.colaborador.equipo
        sede = ctx['object']
        enroladores = m.Enrolador.objects.por_equipo(equipo) \
            .filter(sedes_entrega=sede)

        # Datos de contexto
        enroladores_ctx = list()

        for enrolador in enroladores:
            enroladores_ctx.append({
                'nombre':
                f'{enrolador.nbreve} {enrolador.abreve}',
                'entregados':
                enrolador.sobres_entregados.filter(sede_entrega=sede).count(),
                'escaneados':
                enrolador.sobres_escaneados.filter(sede_entrega=sede).count(),
            })

        ctx['enroladores'] = enroladores_ctx
        ctx['gran_total_entrega'] = EntregaSobre.objects.por_equipo(equipo) \
            .filter(sede_entrega__fecha__lte=self.get_object().fecha).count()
        enrolamiento_totales = Enrolamiento.objects.por_equipo(equipo) \
            .filter(sede__fecha__lte=self.get_object().fecha).values(
                'cantidad_enrol', 'cantidad_actua')
        ctx['gran_total_enrol'] = reduce(
            lambda acum, val: acum + val['cantidad_enrol'],
            enrolamiento_totales, 0)
        ctx['gran_total_actua'] = reduce(
            lambda acum, val: acum + val['cantidad_actua'],
            enrolamiento_totales, 0)

        return ctx


@method_decorator(login_required, 'dispatch')
class CrearSede(v.VistaCrear):
    """ Crea una sede en el sistema """

    model = m.Sede
    template_name = 'crear-sede.html'
    form_class = f.FormSede
    success_url = reverse_lazy('Equipo:Sedes')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se registró la sede correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EditarSede(v.VistaEditar):
    """ Edita una sede en el sistema """

    model = m.Sede
    template_name = 'crear-sede.html'
    form_class = f.FormSede
    success_url = reverse_lazy('Equipo:Sedes')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          editable=True,
                          **self.get_form_kwargs())

    def form_valid(self, form: form_class):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se editó la sede correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class CrearDomiciliaria(CrearSede):
    """ Crea una visita domiciliaria en el sistema """

    form_class = f.FormDomiciliaria


@method_decorator(login_required, 'dispatch')
class EditarDomiciliaria(v.VistaEditar):
    """ Edita una visita domiciliaria en el sistema """

    model = m.Sede
    template_name = 'crear-sede.html'
    form_class = f.FormDomiciliaria
    success_url = reverse_lazy('Equipo:Sedes')

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        return form_class(self.request.user.colaborador.equipo,
                          editable=True,
                          **self.get_form_kwargs())

    def form_valid(self, form):
        """ Añade un mensaje de éxito """

        messages.success(self.request, 'Se editó la sede correctamente')
        return super().form_valid(form)


@method_decorator(login_required, 'dispatch')
class EliminarSede(v.VistaEliminar):
    """ Elimina una sede del sistema """

    model = m.Sede
    template_name = 'eliminar-sede.html'
    success_url = reverse_lazy('Equipo:Sedes')
