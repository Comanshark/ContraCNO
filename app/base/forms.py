""" FORMULARIOS BASE

Se detallan los formularios que serán usados en los otros módulos.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django import forms as f
from app.Equipo.models import Equipo


class FormularioBootstrap(f.Form):
    """ Formulario que añade estilo BS4 """

    def __init__(self, equipo: Equipo, editable: bool = False,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Añadiendo el atributo equipo para filtros
        self.equipo = equipo
        self.editable = editable

        # Añadiendo las clases de BS4
        campos = self.fields.keys()

        for campo in campos:
            if 'class' in self.fields[campo].widget.attrs and \
                    self.fields[campo].widget.attrs['class'] is not None:

                self.fields[campo].widget.attrs['class'] += ' form-control'
            else:
                self.fields[campo].widget.attrs['class'] = 'form-control'

            self.fields[campo].widget.attrs['placeholder'] = \
                self.fields[campo].label

    def _validar_campos(self) -> None:
        """ Marca los los errores y aciertos en los inputs con BS """

        campos = self.fields.keys()

        for campo in campos:
            if campo in self.errors:
                self.fields[campo].widget.attrs['class'] += ' is-invalid'
            else:
                self.fields[campo].widget.attrs['class'] += ' is-valid'

    def clean(self):
        self._validar_campos()
        return super().clean()
