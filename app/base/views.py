""" VISTAS BASE

Se detallan las vistas que se utilizarán en demás módulos.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.shortcuts import redirect
from django.views.generic import (DeleteView, CreateView, ListView, UpdateView,
                                  DetailView, TemplateView)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages

VistaPlantilla = TemplateView


class VistaCrear(LoginRequiredMixin, CreateView):
    pass


class VistaEliminar(LoginRequiredMixin, DeleteView):
    def puede_eliminarlo(self) -> bool:
        colaborador = self.request.user.colaborador

        return colaborador.es_supervisor or colaborador.usuario.is_staff

    def es_equipo(self) -> bool:
        return self.request.user.colaborador.equipo == self.get_object().equipo

    def get_success_url(self):
        if self.success_url:
            return self.success_url
        else:
            return ''

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """ Permiso de eliminación solo al supervisor o enrolador encargado"""

        if not self.es_equipo():
            messages.warning(
                request, 'Usted no pertenece al equipo encargado de esto')
            return redirect(self.get_success_url())

        if self.puede_eliminarlo():
            return super().dispatch(request, *args, **kwargs)
        else:
            messages.error(request,
                           'Usted no tiene permisos para eliminar esto')
            return redirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        """ Envía un mensaje de eliminación exitosa """
        art = 'la' if self.model.__name__[-1] in ('a', 'de',) else 'el'
        model = self.model.__name__.lower()

        messages.success(
            request,
            f'Se ha eliminado {art} {model} correctamente'
        )
        return super().post(request, *args, **kwargs)


class VistaLista(LoginRequiredMixin, ListView):
    """ Agrega la funcionalidad por equipo para la lista """

    def get_queryset(self):
        """ Obtiene los objetos que pertenecen al equipo del usuario """

        return self.model.objects.por_equipo(
            self.request.user.colaborador.equipo)


class VistaEditar(LoginRequiredMixin, UpdateView):
    pass


class VistaVer(LoginRequiredMixin, DetailView):
    pass
