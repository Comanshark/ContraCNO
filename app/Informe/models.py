r""" MODELOS DE INFORMES

Se guardan datos que se utilizan para completar información de los informes.}

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django.db import models
from app.Equipo.models import Sede, Colaborador


class Incidencias(models.Model):
    """ Marca una incidencia en una sede """

    sede = models.OneToOneField(Sede, on_delete=models.CASCADE,
                                related_name='incidencias')
    en_casilla = models.BooleanField(default=False)
    desc = models.CharField('descripcción', max_length=200)


class Inasistencia(models.Model):
    """ Marca las inacistencias de un enrolador """

    sede = models.OneToOneField(Sede, on_delete=models.CASCADE,
                                related_name='inasistentes')
    colaborador = models.ForeignKey(Colaborador, on_delete=models.CASCADE,
                                    related_name='inasistencias')
    respaldo = models.ImageField(upload_to='respaldos/')
