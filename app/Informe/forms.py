r""" FORMULARIOS DE INFORMES

Contiene los formularios utilizados para crear los informes del sistema.

Autor: Carlos E. Rivera
Licencia: GPLv3
"""

from django import forms as f
from app.base.forms import FormularioBootstrap
from . import models as m


class FormIncidencia(FormularioBootstrap, f.ModelForm):
    """ Formulario para editar una incidencia """

    class Meta:
        model = m.Incidencias
        fields = ('en_casilla', 'desc',)


class FormSubirRespaldo(f.ModelForm):
    """ Formulario para subir una imagen a la inasistencia """

    class Meta:
        models = m.Inasistencia
        fields = ('respaldo',)
        widgets = {
            'respaldo': f.FileInput(attrs={'class': 'custom-file-input'})
        }
