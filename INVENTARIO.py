from app.Entrega import models as m


DATOS = dict()

with open('INVENTARIO.csv', 'r') as archivo:
    for linea in archivo.readlines():
        sobre, caja = linea.replace('\n', '').split(';')

        if caja in DATOS and DATOS[caja] is not None:
            DATOS[caja].append(sobre)
        else:
            DATOS[caja] = [sobre]

for caja in DATOS.keys():
    sobres_eliminar = m.Sobre.objects.filter(caja__codigo=caja) \
        .exclude(codigo__in=DATOS[caja]) \
        .exclude(dato_entrega__isnull=False)

    sobres_eliminar.delete()
