/* GUIONES PARA EDITAR CAJA CON ESCANER
 * Este guión muestra la lógica para agregar el código de caja o los sobres
 * que contiene la caja.
 * 
 * Autor: Carlos E. Rivera
 * Licencia: GPLv3
 */

const escaner = new Escaner({sobre: true, caja: true});
let esCaja= true;

// Añadiendo los datos del escaner en los formularios.

$('.btn-primary').click(function() {
    esCaja = true;
});

$('.btn-tool').click(function() {
    esCaja = false;
});


$('#escaner').on('hide.bs.modal', function() {
    
    if (escaner.codigos != undefined && (escaner.codigos.sobre != undefined || escaner.codigos.caja != undefined)) {
        if (esCaja) {
            $('#id_codigo').val(escaner.codigos.caja);
        }
        else {
            form = $('#id_sobres');
            form.val(form.val() + `${escaner.codigos.sobre}\n`);
        }
    }
});
