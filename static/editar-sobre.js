/* GUIÓN PARA EDITAR SOBRES ENTREGADOS
 * Este guión enlaza el escaner con el formulario de creación de entrega.
 * 
 * Autor: Carlos E. Rivera
 * Licencia: GPLv3
 */

const escaner = new Escaner({sobre: true});

// Añadiendo los datos del escaner en los formularios.

$('#escaner').on('hide.bs.modal', () => {
    if (escaner.codigos !== undefined) {
        $('#id_caja').val(escaner.codigos.caja);
        $('#id_sobre').val(escaner.codigos.sobre);
    }
});
