/* GUIÓN DE ESCANER QR
 * 
 * Este guión gestiona el funcionamiento del escaner.
 * 
 * Autor: Carlos E. Rivera
 * Licencia: GPLv3
 */

// REGEXS
const REGEX_SOBRE = /^[0-9]{9}P[0-9]{3}B[0-9]{2}$/;
const REGEX_CAJA = /^P[0-9]{3}B[0-9]{2}$/;
const REGEX_RECIBO = /^[10]{1}[0-9]{3}[1-9]{1}[0-9]{3}[0-9]{5}\|[0-9]{30}$/


class Escaner
{
    constructor(params) {
        
        // Inicializando área del escaner.
        this.area_escaner = new Html5Qrcode('area-escaner');
        this.sobre = params.sobre != undefined ? params.sobre : false;
        this.caja = params.caja != undefined ? params.caja : false;
        this.recibo = params.recibo != undefined ? params.recibo : false;
        
        // Añadiendo eventos del modal.
        $('#escaner').on('show.bs.modal', () => this.iniciarEscaner());
        $('#escaner').on('hide.bs.modal', () => this.detenerEscaner());
    }

    codigoValido = mensajeQr => {
        
        if (this.sobre && REGEX_SOBRE.test(mensajeQr)) {
        console.log(mensajeQr);
            this.codigos = {
                sobre: mensajeQr.substring(0, 9),
                caja: mensajeQr.substring(9, 16)           
            };
            $('#escaner').modal('hide');
        }
        else if (this.caja && REGEX_CAJA.test(mensajeQr)) {
            this.codigos = {caja: mensajeQr};
            $('#escaner').modal('hide');
        }
        else if (this.recibo && REGEX_RECIBO.test(mensajeQr)) {
            this.codigos = {recibo: mensajeQr.substring(14, 44)};
            $('#escaner').modal('hide');
        }
    }
                
    iniciarEscaner() {
        Html5Qrcode.getCameras().then(devices => {
            if (devices && devices.length) {
                var cameraId = devices[devices.length - 1].id;
                this.area_escaner.start(cameraId,
                                   {fps:20, qrbox: 125},
                                   this.codigoValido);
            }
        }).catch(err => {
            Swal.fire({
            title: '¡Ups!',
            text: 'Al parecer hubo un error con la cámara.\n' + err,
            icon: 'error'});
        });
    }
    
    detenerEscaner() {
        this.area_escaner.stop();
        $('#area-escaner').children().remove();
    }
}
