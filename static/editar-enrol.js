/* GUIONES PARA EDITAR ENROLAMIENTO CON ESCANER
 * 
 * Este guión funciona para escanear los dos recibos de enrolamientos del día.
 * 
 * Autor: Carlos E. Rivera
 * Licencia: GPLv3
 */

const escaner = new Escaner({ recibo: true });
let campo = $('#id_primer_recibo');

// Añadiendo los datos del escaner en los formularios.

$('#inicio').click(function() {
    campo = $('#id_primer_recibo');
    $('#escaner').modal('show');
});

$('#final').click(function() {
    campo = $('#id_ultimo_recibo');
    $('#escaner').modal('show');
});

$('#escaner').on('hide.bs.modal', () => {
    if (escaner.codigos != undefined && escaner.codigos.recibo != undefined)
        campo.val(escaner.codigos.recibo);
});
